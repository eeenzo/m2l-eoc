import { Link } from "react-router-dom";
import { useState } from 'react';
import axios from 'axios';
import { Navigate } from 'react-router-dom';

export default function UserInterface(props) {
  // Récupérez les informations passées en tant que props
  const { isLoggedIn, userData } = props;
  
  console.log(userData)
  const [last_name, setLastName] = useState(userData.last_name);
  const [first_name, setFirstName] = useState(userData.first_name);
  const [address, setAddress] = useState(userData.adress);
  const [email, setEmail] = useState(userData.email);
  const [phone, setPhone] = useState(userData.phone);
  const [password, setPwd] = useState(userData.password);

  const handleSubmit = (event) => {
    event.preventDefault();
  
    axios.post('http://localhost:8000/update_user', {
      last_name: last_name,
      first_name: first_name,
      address: address,
      email: email,
      phone: phone,
      password: password,
      id: userData.id
    })
    .then(response => {
      if (response) {
        window.location.reload();
        // localStorage.setItem('token', response.data.token); // mettons à jour les données de l'utilisateur a jours lorsque on verifie le token ?
      }
    });
  }

  const handleSubmitLogout = (event) => {
    event.preventDefault();
  
    axios.post('http://localhost:8000/client_logout', {
      token: localStorage.getItem('token'),
      idClient: JSON.parse(localStorage.getItem('userData')).idClient,
    })
    .then(response => {
    });
    localStorage.removeItem('token');
    localStorage.removeItem('userData');
    window.location.replace("http://localhost:3000/");
  }

  if (isLoggedIn === false) {
    <Navigate replace to="/login" />
  }
  

  return JSON.parse(userData).admin === 1 ? (<Navigate replace to="/adminmanagement" />) : (
    <>
      <h1>UserInterface</h1>


      <button onClick={handleSubmitLogout}>Déconnexion</button>
      <form onSubmit={handleSubmit}>
            <p>
                <label htmlFor="name">Nom :</label><br />
                <input type="text" id="name" name="usr_name" value={JSON.parse(userData).surname} onChange={e => setLastName(e.target.value)}/>
            </p>
            <br />
            <p>
                <label htmlFor="firstname">Prénom :</label><br />
                <input type="text" id="firstname" name="usr_firstname" value={JSON.parse(userData).first_name} onChange={e => setFirstName(e.target.value)}/>
            </p>
            <br />
            <p>
                <label htmlFor="address">Adresse :</label><br />
                <input type="text" id="address" name="usr_address" value={JSON.parse(userData).adress} onChange={e => setAddress(e.target.value)}/>
            </p>
            <br />
            <p>
                <label htmlFor="email">E-mail :</label><br />
                <input type="email" id="email" name="usr_email" value={JSON.parse(userData).email} onChange={e => setEmail(e.target.value)}/>
            </p>
            <br />
            <p>
                <label htmlFor="phone">Téléphone :</label><br />
                <input type="tel" id="phone" name="usr_phone" value={JSON.parse(userData).phone} onChange={e => setPhone(e.target.value)}/>
            </p>
            <br />
            <p>
                <label htmlFor="password">Password :</label><br />
                <input type="password" id="password" name="usr_password" value={JSON.parse(userData).password} onChange={e => setPwd(e.target.value)}/>
            </p>
            <p className="submit">
                <input type="submit" />
            </p>
        </form>

        <Link to="/userordered">Voir l'historique des commandes</Link>
    </>
    );
}