import "../styles/About.css"

function About() {
    return(<div className="main-ctnt">
            <iframe title="maps" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d10533.630133577997!2d6.21459!3d48.6976911!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479498337fd326ab%3A0x193970a28747751e!2sMaison%20R%C3%A9gionale%20des%20Sports!5e0!3m2!1sfr!2sfr!4v1682502637472!5m2!1sfr!2sfr" referrerpolicy="no-referrer-when-downgrade"></iframe>
            <form>
                <p>
                    <label for="fname">First name :</label><br/>
                    <input type="text" id="fname" name="usr_fname"/>
                </p>
                <p>
                    <label for="lname">Last name :</label><br/>
                    <input type="text" id="lname" name="usr_lname"/>
                </p>
                <p>
                    <label for="email">E-mail :</label><br/>
                    <input type="email" id="email" name="usr_email"/>
                </p>
                <p>
                    <label for="suggestion">Suggestion :</label><br/>
                    <textarea id="suggestion" name="usr_sgstn"></textarea>
                </p>
                <p className="submit">
                    <input type="submit" />
                </p>
            </form>
        </div>
    )

}


export default About;