import "../styles/Product.css";
// import BlockMystery from "../component/BlockMystery"
import ProductListe from "../component/ProductListe"

function Product() {
    return <div className="ctnt-product-page">
        {/* <div className="ctnt-mystery">
            <BlockMystery />
        </div> */}
        <div className="ctnt-product-liste">
            <ProductListe />
        </div>
    </div>

}


export default Product;