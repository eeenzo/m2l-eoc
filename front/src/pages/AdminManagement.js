import '../styles/AdminManagement.css';
import React, { useState, useEffect } from 'react';
import axios from 'axios';

export default function AdminManagement() {
    const [users, setUsers] = useState([]);
    const [products, setProducts] = useState([]);
    const [newUser, setNewUser] = useState({
      idClient: "",
      first_name: "",
      surname: "",
      address: "",
      email: "",
      phone: "",
      admin: false
    });
    const [newProduct, setNewProduct] = useState({
      id_product: "",
      name: "",
      price: "",
      stock: "",
      description: "",
      id_category: "",
      img_name: ""
    });

    useEffect(() => {
      axios.get('http://localhost:8000/users')
        .then(response => {
          setUsers(response.data);
        })
        .catch(error => {
          console.log(error);
        });
        axios.get('http://localhost:8000/products')
        .then(response => {
          setProducts(response.data);
        })
        .catch(error => {
          console.log(error);
        });
    }, []);
  
    const handleDeleteUser = (id) => {
      axios.delete(`http://localhost:8000/users/${id}`)
        .then(response => {
          setUsers(users.filter(user => user.idClient !== id));
        })
        .catch(error => {
          console.log(error);
        });
    }
  
    const handleDeleteSelectedUsers = () => {
        const selectedUsers = users.filter(user => user.isSelected);
      
        if (selectedUsers.length > 0) {
          const ids = selectedUsers.map(user => user.idClient);
      
          axios.delete('http://localhost:8000/delete-selected-users', {
            data: { ids: ids }
          })
            .then(response => {
              setUsers(users.filter(user => !user.isSelected));
            })
            .catch(error => {
              console.log(error);
            });
        }
      }
  
    const handleSelectUser = (id) => {
      setUsers(users.map(user => {
        if (user.idClient === id) {
          user.isSelected = !user.isSelected;
        }
        return user;
      }));
    }

    const handleAddUser = (event) => {
      event.preventDefault();
      axios.post('http://localhost:8000/adminAddUser', newUser)
      .then(response => {
        updateUsers();
      })
        .catch(error => {
          console.log(error);
        });
    }

    const handleEditUser = () => {
      axios.put(`http://localhost:8000/adminEditUser/${newUser.idClient}`, newUser)
        .then(response => {
          updateUsers();
        })
        .catch(error => {
          console.log(error);
        });
    }

    function updateUsers() {
      axios.get('http://localhost:8000/users')
        .then(response => {
          setUsers(response.data);
          setNewUser({
            idClient: "",
            first_name: "",
            surname: "",
            address: "",
            email: "",
            phone: "",
            admin: false
          });
        })
        .catch(error => {
          console.log(error);
        });
    };

    const handleAddProduct = (event) => {
      event.preventDefault();
      console.log(newProduct);
      axios.post('http://localhost:8000/adminAddProduct', newProduct)
      .then(response => {
        console.log(response);
        updateProduct();
      })
        .catch(error => {
          console.log(error);
        });
    }

    const handleEditProduct = () => {
      axios.put(`http://localhost:8000/adminEditProduct/${newUser.idClient}`, newProduct)
        .then(response => {
          updateProduct();
        })
        .catch(error => {
          console.log(error);
        });
    }

    function updateProduct() {
      axios.get('http://localhost:8000/products')
        .then(response => {
          setProducts(response.data);
          setNewProduct({
            id_product: "",
            name: "",
            price: "",
            stock: "",
            description: "",
            id_category: "",
            img_name: ""
          });
        })
        .catch(error => {
          console.log(error);
        });
    };

    const handleSubmitLogout = (event) => {
      event.preventDefault();
    
      axios.post('http://localhost:8000/client_logout', {
        token: localStorage.getItem('token'),
        idClient: JSON.parse(localStorage.getItem('userData')).idClient,
      })
      .then(response => {
      });
      localStorage.removeItem('token');
      localStorage.removeItem('userData');
      window.location.replace("http://localhost:3000/");
    }

    return (<>
        <h1>Interface administrateur</h1>

        <button onClick={handleSubmitLogout}>Déconnexion</button>

        <div className='userManager'>
            <h2>Gestion des utilisateurs</h2>
            <table>
                <thead>
                <tr>
                    <th>Sélectionner</th>
                    <th>ID</th>
                    <th>Prénom</th>
                    <th>Nom</th>
                    <th>Adresse</th>
                    <th>Email</th>
                    <th>Téléphone</th>
                    <th>Admin</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                {users.map(user => (
                    
                    <tr key={user.idClient}>
                        <td>
                            <input type="checkbox" checked={user.isSelected} onChange={() => handleSelectUser(user.idClient)} />
                        </td>
                        <td>{user.idClient}</td>
                        <td>{user.first_name}</td>
                        <td>{user.surname}</td>
                        <td>{user.adress}</td>
                        <td>{user.email}</td>
                        <td>{user.phone}</td>
                        <td>{user.admin ? 'Oui' : 'Non'}</td>
                        <td>
                            <button onClick={() => handleDeleteUser(user.idClient)}>Supprimer</button>
                        </td>
                    </tr>
                ))}
                <tr key="admin-add-user">
                    <td>ADD</td>
                    <td></td>
                    <td><input type="text" onChange={(event) => setNewUser({ ...newUser, first_name: event.target.value })} /></td>
                    <td><input type="text" onChange={(event) => setNewUser({ ...newUser, surname: event.target.value })} /></td>
                    <td><input type="text" onChange={(event) => setNewUser({ ...newUser, address: event.target.value })} /></td>
                    <td><input type="text" onChange={(event) => setNewUser({ ...newUser, email: event.target.value })} /></td>
                    <td><input type="number" onChange={(event) => setNewUser({ ...newUser, phone: event.target.value })} /></td>
                    <td><input type="checkbox" onChange={(event) => setNewUser({ ...newUser, admin: event.target.checked })} /></td>
                    <td><button onClick={handleAddUser}>Ajouter</button></td>
                </tr>
                <tr key="admin-edit-user">
                    <td>EDIT</td>
                    <td><input type="number" onChange={(event) => setNewUser({ ...newUser, idClient: event.target.value })} /></td>
                    <td><input type="text" onChange={(event) => setNewUser({ ...newUser, first_name: event.target.value })} /></td>
                    <td><input type="text" onChange={(event) => setNewUser({ ...newUser, surname: event.target.value })} /></td>
                    <td><input type="text" onChange={(event) => setNewUser({ ...newUser, address: event.target.value })} /></td>
                    <td><input type="text" onChange={(event) => setNewUser({ ...newUser, email: event.target.value })} /></td>
                    <td><input type="number" onChange={(event) => setNewUser({ ...newUser, phone: event.target.value })} /></td>
                    <td><input type="checkbox" onChange={(event) => setNewUser({ ...newUser, admin: event.target.checked })} /></td>
                    <td><button onClick={handleEditUser}>Modifier</button></td>
                </tr>
                </tbody>
            </table>
            <button onClick={handleDeleteSelectedUsers}>Supprimer les utilisateurs sélectionnés</button>
      </div>

      <br/><br/><br/><br/>

      <div className='productManager'>
        <h2>Gestion des produits</h2>
        <table>
                <thead>
                <tr>
                    <th>Sélectionner</th>
                    <th>ID</th>
                    <th>Nom</th>
                    <th>Prix</th>
                    <th>QTY</th>
                    <th>Description</th>
                    <th>Categorie</th>
                    <th>Nom d'Image</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                {products.map(product => (
                    
                    <tr key={product.id_product}>
                        <td>
                            <input type="checkbox" checked={product.isSelected} onChange={() => handleSelectUser(product.id_product)} />
                        </td>
                        <td>{product.id_product}</td>
                        <td>{product.name}</td>
                        <td>{product.price}</td>
                        <td>{product.stock}</td>
                        <td>{product.description}</td>
                        <td>{product.id_category}</td>
                        <td>{product.img_name}</td>
                        <td>
                            <button onClick={() => handleDeleteUser(product.id_product)}>Supprimer</button>
                        </td>
                    </tr>
                ))}
                <tr key="admin-add-user">
                    <td>ADD</td>
                    <td></td>
                    <td><input type="text" onChange={(event) => setNewProduct({ ...newProduct, name: event.target.value })} /></td>
                    <td><input type="number" onChange={(event) => setNewProduct({ ...newProduct, price: event.target.value })} /></td>
                    <td><input type="number" onChange={(event) => setNewProduct({ ...newProduct, stock: event.target.value })} /></td>
                    <td><input type="text" onChange={(event) => setNewProduct({ ...newProduct, description: event.target.value })} /></td>
                    <td><input type="number" onChange={(event) => setNewProduct({ ...newProduct, id_category: event.target.value })} /></td>
                    <td><input type="text" onChange={(event) => setNewProduct({ ...newProduct, img_name: event.target.value })} /></td>
                    <td><button onClick={handleAddProduct}>Ajouter</button></td>
                </tr>
                <tr key="admin-edit-user">
                    <td>EDIT</td>
                    <td><input type="number" onChange={(event) => setNewProduct({ ...newProduct, id_product: event.target.value })} /></td>
                    <td><input type="text" onChange={(event) => setNewProduct({ ...newProduct, name: event.target.value })} /></td>
                    <td><input type="number" onChange={(event) => setNewProduct({ ...newProduct, price: event.target.value })} /></td>
                    <td><input type="number" onChange={(event) => setNewProduct({ ...newProduct, stock: event.target.value })} /></td>
                    <td><input type="text" onChange={(event) => setNewProduct({ ...newProduct, description: event.target.value })} /></td>
                    <td><input type="number" onChange={(event) => setNewProduct({ ...newProduct, id_category: event.target.value })} /></td>
                    <td><input type="text" onChange={(event) => setNewProduct({ ...newProduct, img_name: event.target.value })} /></td>
                    <td><button onClick={handleEditProduct}>Modifier</button></td>
                </tr>
                </tbody>
            </table>
            <button onClick={handleDeleteSelectedUsers}>Supprimer les produits sélectionnés</button>
      </div>
      <br/><br/>
    </>)
}