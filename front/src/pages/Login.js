import '../styles/Login.css';
import { useState } from 'react';
import axios from 'axios';
import { Navigate } from 'react-router-dom';


export default function Login() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [haveError, setHaveError] = useState(false);
    const [error, setError] = useState('');
    const isLoggedIn = localStorage.getItem('token') !== null;

    const [last_name, setLastName] = useState('');
    const [first_name, setFirstName] = useState('');
    const [address, setAddress] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [passwordTwo, setPwd] = useState('');

    const handleSubmit = (event) => {
      event.preventDefault();

      axios.post('http://localhost:8000/client_login', { username, password })
      .then(response => {
        if (response.data.success) {
          localStorage.setItem('token', response.data.token);
          localStorage.setItem('userData', JSON.stringify(response.data.userData));
          // console.log("ok");
          if (localStorage.getItem('token') && localStorage.getItem('userData')) {
            console.log(response.data.userData)
            console.log(localStorage.getItem('userData'))
            
            console.log("connecter");
            window.location.reload();
          }
        } else {
          setHaveError(true);
          setError(response.data.error);
          console.log("not ok");
        }
      })
  };

  const handleSubmitTwo = (event) => {
    event.preventDefault();
    axios.post('http://localhost:8000/CreateUser', { last_name, first_name, address, email, phone, passwordTwo })
      .then(response => {
        if (response.data.success) {
          localStorage.setItem('token', response.data.token);
          localStorage.setItem('userData', JSON.stringify(response.data.userData));
          // console.log("ok");
          if (localStorage.getItem('token') !== null) {
            window.location.reload();
            console.log("connecter");
          }
        } else {
          setHaveError(true);
          setError(response.data.error);
          console.log("not ok");
        }
      })
  };
      
    return isLoggedIn ? (<Navigate replace to="/userinterface" />) : (
        <>
          <h1>Login</h1>
          {haveError ? <p>erreur :{error}</p> : <p>pas d'erreur</p>}
          <form onSubmit={handleSubmit}>
            <p>
              <label htmlFor="email">E-mail :</label><br />
              <input type="email" id="email" name="usr_email" onChange={e => setUsername(e.target.value)} />
            </p>
            <p>
              <label htmlFor="password">Password :</label><br />
              <input type="password" id="password" name="usr_password" onChange={e => setPassword(e.target.value)} />
            </p>
            <p className="submit">
              <input type="submit"/>
            </p>
          </form>
          <p>{localStorage.getItem('token')}</p>


          <br/><br/><br/><br/><br/>


          <h1>Créer un compte</h1>
          <form onSubmit={handleSubmitTwo}>
            <p>
                <label htmlFor="nameTWO">Nom :</label><br />
                <input type="text" id="nameTWO" name="cr_name" value={last_name} onChange={event => setLastName(event.target.value)}/>
            </p>
            <p>
                <label htmlFor="firstnameTWO">Prénom :</label><br />
                <input type="text" id="firstnameTWO" name="cr_firstname" value={first_name} onChange={event => setFirstName(event.target.value)}/>
            </p>
            <p>
                <label htmlFor="addressTWO">Adresse :</label><br />
                <input type="text" id="addressTWO" name="ce_address" value={address} onChange={event => setAddress(event.target.value)}/>
            </p>
            <p>
                <label htmlFor="emailTWO">E-mail :</label><br />
                <input type="email" id="emailTWO" name="cr_email" value={email} onChange={event => setEmail(event.target.value)}/>
            </p>
            <p>
                <label htmlFor="phoneTWO">Téléphone :</label><br />
                <input type="tel" id="phoneTWO" name="cr_phone" value={phone} onChange={event => setPhone(event.target.value)}/>
            </p>
            <p>
                <label htmlFor="passwordTWO">Password :</label><br />
                <input type="password" id="passwordTWO" name="cr_password" value={passwordTwo} onChange={event => setPwd(event.target.value)}/>
            </p>
            <p className="submit">
              <input type="submit" />
            </p>
        </form>
        </>
      )
    }