import '../styles/Basket.css';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { Navigate } from 'react-router-dom';

function Basket() {
  const [products, setProducts] = useState([]);
  const [affichage, setAffichage] = useState(false);

  useEffect(() => {
    const existingCart = JSON.parse(localStorage.getItem('cart') || '[]');
    axios.get(`http://localhost:8000/products`)
      .then((res) => {
        const filteredProducts = [];
        for (let i = 0; i < res.data.length; i++) {
          const product = res.data[i];
          if (existingCart.some((item) => parseInt(item.id) === product.id_product)) {
            filteredProducts.push(product);
          }
        }
        setProducts(filteredProducts);
        setAffichage(true);
      });
  }, []);

  const getQuantityFromCart = (productId) => {
    const existingCart = JSON.parse(localStorage.getItem('cart') || '[]');
    const product = existingCart.find((item) => parseInt(item.id) === parseInt(productId));
    return product ? product.quantity : 1; // 1 est la valeur par défaut si le produit n'est pas trouvé dans cart
  }

  const handleQuantityChange = (productId, quantity) => {
    const existingCart = JSON.parse(localStorage.getItem('cart') || '[]');
    const index = existingCart.findIndex((item) => parseInt(item.id) === parseInt(productId));
    const newQuantity = parseInt(quantity); // conversion de la chaîne de caractères en nombre
    if (newQuantity > 0) {
      if (index >= 0) {
        existingCart[index].quantity = newQuantity;
      } else {
        existingCart.push({ id: productId, quantity: newQuantity });
      }
      localStorage.setItem('cart', JSON.stringify(existingCart));
    } else {
      if (index >= 0) {
        existingCart.splice(index, 1);
        localStorage.setItem('cart', JSON.stringify(existingCart));
      }
      setProducts(products.filter((product) => parseInt(product.id_product) !== parseInt(productId)));
    }
  }
  

  return !localStorage.getItem('token') ? (<Navigate replace to="/Login" />) : (
    <div className="product-liste">
      {affichage ? products.map(item => (
        <div className="product-card" key={item.id_product}>
          <div className="product-card-ctnt">
            <div className="out_img">
              <img src={ require("../assets/" + item.img_name)} alt={"../assets/" + item.img_name } />
            </div>
            <p className="product-name">{item.name}</p>
            <input type="number" min="0" defaultValue={getQuantityFromCart(item.id_product)} max={item.stock} onChange={(e) => handleQuantityChange(item.id_product, e.target.value)}/>
          </div>
        </div>
      )) : <p>Chargement...</p> }
    </div>
  );
}

export default Basket;