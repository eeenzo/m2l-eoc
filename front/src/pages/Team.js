import "../styles/Team.css"
import dfltUserImg from "../assets/teacher-2.jpg";
// import {Link} from "react-router-dom";

function Team() {
    return <section className="team-ctnt">
        <div className="team-card">
            <img src={dfltUserImg} alt="Profile de user1" />
            <p className="member-name">Martin Leclerc</p>
            <p className="member-desc">Martin est un expert en matière de finances et de comptabilité. Il est responsable de la gestion des budgets de la Maison des Liges de Lorraine, ainsi que de la supervision des comptes et des dépenses.</p>
            {/* <Link to="/home">link</Link> */}
        </div>
        <div className="team-card">
            <img src={dfltUserImg} alt="Profile de user2" />
            <p className="member-name">Émilie Dupont</p>
            <p className="member-desc">Émilie est une organisatrice hors pair. Elle est chargée de coordonner tous les événements organisés par la Maison des Liges de Lorraine, tels que des conférences, des expositions ou des galas.</p>
            {/* <Link to="/home">link</Link> */}
        </div>
        <div className="team-card">
            <img src={dfltUserImg} alt="Profile de user3" />
            <p className="member-name">Pierre Lambert</p>
            <p className="member-desc">Pierre est un communicant chevronné. Il est chargé de promouvoir les activités de la Maison des Liges de Lorraine auprès du grand public et des médias, en utilisant des stratégies de communication innovantes et efficaces.</p>
            {/* <Link to="/home">link</Link> */}
        </div>
    </section>
}


export default Team;