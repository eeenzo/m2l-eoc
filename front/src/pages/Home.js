import "../styles/Home.css"
import illu from "../assets/home.png";
// import { Redirect } from "react-router-dom";

function Home() {

    return <section className="home-ctnt">
        <div className="text">
            <h1>Bienvenue !</h1>
            {/* <h1>Bienvenue {userToken}</h1> */}
            <p>Sur le site e-commerce de la M2L</p>
            
        </div>
        <div className="illu">
            <img src={illu} alt="Illustrations des produit proposer"/>
            <div className="illu-support"></div>
        </div>
    </section>
}


export default Home;