import { useState, useEffect } from 'react';
import axios from 'axios';

export default function OrdersList() {
    const [orders, setOrders] = useState([]);
    const [affichage, setAffichage] = useState(false);

  useEffect(() => {
    const idClient = JSON.parse(localStorage.getItem('userData')).idClient;
    axios.get(`http://localhost:8000/getOrders/${idClient}`)
      .then(res => {
        setOrders(res.data);
        setAffichage(true);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <h1>Liste des commandes</h1>
      <table>
        <thead>
          <tr>
            <th>Date</th>
            <th>Identifiant</th>
          </tr>
        </thead>
        <tbody>
            {affichage ?
                orders.map(order => (
                    <tr key={order.designation}>
                    <td>{order.date}</td>
                    <td>{order.designation}</td>
                    </tr>
                ))
            : <p>Chargement...</p>}
        </tbody>
      </table>
    </div>
  );
}