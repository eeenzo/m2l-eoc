import { Routes, Route } from "react-router-dom";
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import '../styles/App.css';
import Header from "../component/Header"
import Footer from "../component/Footer"
import Home from "../pages/Home"
import About from "../pages/About"
import Team from "../pages/Team"
import Product from "../pages/Product"
import AdminManagement from "../pages/AdminManagement"
import AdminOrderHistory from "../pages/AdminOrderHistory"
import Associations from "../pages/Associations"
import Basket from "../pages/Basket"
import CGVCGU from "../pages/CGVCGU"
import Contacts from "../pages/Contacts"
import LegalNotice from "../pages/LegalNotice"
import Login from "../pages/Login"
import Productsdetail from "../pages/Productsdetail"

import UserInterface from "../pages/UserInterface"
import UserOrdered from "../pages/UserOrdered"
// import { useState } from "react";




function App() {

  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [userData, setUserData] = useState(localStorage.getItem('userData'));
  const [userToken, setUserToken] = useState('');

  useEffect(() => {
    const localToken = localStorage.getItem('token');
    setUserToken(localToken)
    // console.log("set token");
    // console.log(localToken);
    if (localToken) {
      // console.log("if local token");
      // console.log(localToken);
      axios.post('http://localhost:8000/verify_token', {
        localToken:localToken
      })
      .then(response => {
        if (response.data.success) {
          // console.log("respons success");
          setIsLoggedIn(true);
          console.log(response.data.userData)
          setUserData(JSON.stringify(response.data.userData));
          localStorage.setItem('userData', JSON.stringify(response.data.userData));
        }
      });
    }
  }, []);


  return (
      <div>
        <div className="main">
          <Header isLoggedIn={isLoggedIn} userData={userData} userToken={userToken}/>
          <Routes>
            <Route path="/" element={<Home isLoggedIn={isLoggedIn} userData={userData} userToken={userToken} />} />
            <Route path="/about" element={<About />} />
            <Route path="/team" element={<Team />} />
            <Route path="/product" element={<Product />} />
            <Route path="/login" element={<Login />} />

            <Route path="/adminmanagement" element={<AdminManagement isLoggedIn={isLoggedIn} userData={userData} userToken={userToken}/>} />
            <Route path="/adminorderhistory" element={<AdminOrderHistory isLoggedIn={isLoggedIn} userData={userData} userToken={userToken}/>} />
            <Route path="/associations" element={<Associations />} />
            <Route path="/basket" element={<Basket />} />
            <Route path="/cgvcgu" element={<CGVCGU />} />
            <Route path="/contacts" element={<Contacts />} />
            <Route path="/legalnotice" element={<LegalNotice />} />
            <Route path="/productsdetail" element={<Productsdetail />} />
            <Route path="/userinterface" element={<UserInterface isLoggedIn={isLoggedIn} userData={userData} userToken={userToken}/>} />
            <Route path="/userordered" element={<UserOrdered isLoggedIn={isLoggedIn} userData={userData} userToken={userToken}/>} />
          </Routes>
        </div>
        <Footer />
      </div>
  );
}

export default App;