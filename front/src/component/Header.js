import "../styles/Header.css"
import m2l_logo_2 from "../assets/logo_M2L_2.png"
import { Link } from "react-router-dom";
import React from 'react'
import App from "../component/App"
// import { useEffect, useState } from 'react';

function Header() {
    // const [cartCount, setCartCount] = useState(0);

    // useEffect(() => {
    //     const existingCart = JSON.parse(localStorage.getItem('cart'));
    //     setCartCount(existingCart.length);
    // }, []);

    return  <header>
                <div className={`${App.darkTheme ? 'dark-theme' : 'light-theme'} top-separator`}></div>
                <div className="header-ctnt">
                    <div className="header_logo_name">
                        <img src={m2l_logo_2} alt="logo" />
                        <p className="header_name">Maison des Ligues</p>
                    </div>
                    <nav>
                        <div className="nav">
                            <ul>
                                <li className="tab tab1"><Link to="/">Home<span className="nav-undernile"></span></Link></li>
                                <li className="tab tab2"><Link to="/product">Product<span className="nav-undernile"></span></Link></li>
                                <li className="tab tab3"><Link to="/team">Team<span className="nav-undernile"></span></Link></li>
                                <li className="tab tab5"><Link to="/about">Contact Us<span className="nav-undernile"></span></Link></li>
                                <li className="tab tab6 tab_login">
                                    <Link className="btn_cart" to="/basket">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 24 24"><g id="vuesax_linear_bag" data-name="vuesax/linear/bag" transform="translate(-108 -188)"><g id="bag"><path id="Vector" d="M3.62,0,0,3.63" transform="translate(113.19 190)" fill="none" stroke="#292d32" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/><path id="Vector-2" data-name="Vector" d="M0,0,3.62,3.63" transform="translate(123.19 190)" fill="none" stroke="#292d32" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/><path id="Vector-3" data-name="Vector" d="M0,2C0,.15.99,0,2.22,0H17.78C19.01,0,20,.15,20,2c0,2.15-.99,2-2.22,2H2.22C.99,4,0,4.15,0,2Z" transform="translate(110 193.85)" fill="none" stroke="#292d32" stroke-width="2"/><path id="Vector-4" data-name="Vector" d="M0,0V3.55" transform="translate(117.76 202)" fill="none" stroke="#292d32" stroke-linecap="round" stroke-width="2"/><path id="Vector-5" data-name="Vector" d="M0,0V3.55" transform="translate(122.36 202)" fill="none" stroke="#292d32" stroke-linecap="round" stroke-width="2"/><path id="Vector-6" data-name="Vector" d="M0,0,1.41,8.64C1.73,10.58,2.5,12,5.36,12h6.03c3.11,0,3.57-1.36,3.93-3.24L17,0" transform="translate(111.5 198)" fill="none" stroke="#292d32" stroke-linecap="round" stroke-width="2"/><path id="Vector-7" data-name="Vector" d="M0,0H24V24H0Z" transform="translate(108 188)" fill="none" opacity="0"/></g></g></svg>
                                        {/* {cartCount ? <div className="count-cart"><p>{cartCount}</p></div> : null} */}
                                    </Link>
                                    <Link className="btn_login" to="/Login"><p>{localStorage.getItem('token') ? 'My Account' : 'Login'}</p></Link>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </header>
}


export default Header;