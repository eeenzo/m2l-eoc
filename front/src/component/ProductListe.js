import "../styles/ProductListe.css";
import { useEffect, useState } from 'react';
import axios from 'axios';

function ProductListe() {
  const [Product, setProduct] = useState([])
  const [affichage, setAffichage] = useState(false)


  const recup = async () => {
    await axios.get(`http://localhost:8000/products`)
    .then(res => {
      setProduct(res.data)
      setAffichage(true)
    })
  }
  useEffect(()=>{
    recup()
  },[])

  const addToCart = (id, quantity) => {
    const existingCart = JSON.parse(localStorage.getItem('cart') || '[]');
    const productIndex = existingCart.findIndex(product => product.id === id);
    const qty = parseInt(quantity, 10);
    if (productIndex === -1) {
      existingCart.push({ id, quantity: qty });
    } else {
      existingCart[productIndex].quantity += qty;
    }
    localStorage.setItem('cart', JSON.stringify(existingCart));
  };  
  const existingCart = JSON.parse(localStorage.getItem('cart') || '[]');
  

    return (
    <div className="product-liste">
        {affichage ? Product.map(item => (
            <div className="product-card" key={item.id_product}>
                <div className="product-card-ctnt">
                        <div className="out_img">
                            <img src={ require("../assets/" + item.img_name)} alt={"../assets/" + item.img_name} />
                        </div>
                        <p className="product-name">{item.name}</p>
                        <p className="product-desc">{item.description}</p>
                        <p className="product-rating">Rating : {item.rating}/5</p>
                        <button onClick={() => addToCart(item.id_product, document.getElementById(`qty${item.id_product}`).value)}>{localStorage.getItem('token') ? existingCart.includes(item.id_product) ? "Already in basket !" :
                          <>
                            {item.stock ? "Add to Cart - "+ item.price +"€" : "Out of Stock - "+ item.price +"€"}
                          </>
                          : "Log in to buy !"
                        }</button>
                        <input id={`qty${item.id_product}`} type="number" min="1" defaultValue={1} max={item.stock}/>
                </div>
            </div>
        ))
        : <p>Chargement...</p> }
    </div>
    );
}

export default ProductListe;