import '../styles/App.css';
import React from 'react'
import m2l_logo_2 from "../assets/logo_M2L_2.png"
import "../styles/Footer.css"

function Footer(props) {
  return (<footer>
        <img className='footer-logo' src={m2l_logo_2} alt="Logo - M2L" />
    </footer>
  );
}

export default Footer;