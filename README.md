# M2L - Maison des Ligues de Lorraine

M2L est une application web développée pour la Maison des Ligues de Lorraine. Elle permet aux différentes ligues sportives régionales d'organiser et de gérer leurs événements et leurs réservations de salles.

## Installation

1. Cloner le dépôt git : `git clone https://github.com/votre-utilisateur/m2l.git`
2. Installer les dépendances dans `/API` et `/front` : `npm install`
3. Configurer les variables d'environnement en créant un fichier `.env` à la racine du fichier `/API` avec les informations suivantes :

```
DB_HOST=127.0.0.1
DB_DTB=m2l_db
DB_USER=[your-username]
DB_PWD=[your-password]
```

4. Importer la base de donnée `m2l_db.sql` situer dans `/API` avec le nom `m2l_db`.
5. Démarrer l'application : `npm start`

## Utilisation

Une fois l'application lancée, vous pouvez accéder à l'interface web à l'adresse suivante : `http://localhost:3000`. 

L'application dispose de deux types d'utilisateurs : les administrateurs et les utilisateurs. Les administrateurs peuvent ajouter et gérer les produit/utilisateur de la M2L, tandis que les membres peuvent effectuer des achat.

### Utilisateurs fictifs

|              |USERNAME            |PASSWORD |
|--------------|--------------------|---------|
|Administrateur|`gord.fr@gmail.com` |`1dshf4` |
|Membres       |`arth.mor@gmail.com`|`gsfhgf2`|

## Fonctionnalités

- Authentification des utilisateurs
- Gestion des produits et utilisateurs
- Visualisation des achats et des produits

## Auteurs

- Enzo Espinasse (https://www.linkedin.com/in/enzo-espinasse/)
- Oscar Zou (https://www.linkedin.com/in/oscar-zou-348243229/)
- Clément Sacco (https://www.linkedin.com/in/cl%C3%A9ment-sacco-014578223/)